// 3-4.
let getCube = (a, b) => {
    console.log(`The cube of ${a} is ${a**b}`)
};
getCube(2,3);

// 5-6.
address = [258, "Washington Ave", "NW", "California", 90011];
const [stNum, St, state, zipCode] = address;
console.log(`I live at ${stNum} ${St}. ${state}, ${zipCode}`)

// 7-8.
let animal = {
    name: "Lolong",
    type: "saltwater crocodile",
    wt: 1075,
    measure: "20 ft 3 in"
}
const {name, type, wt, measure} = animal;
console.log(`${name} was a ${type}. He weighted at ${wt} kgs with a measurement of ${measure}`)

// 9-10.
let num = [1,2,3,4,5,15,];
num.forEach(x => console.log(x));

// 11.
let reduce = console.log(num.reduce((a,b) => a+b));
reduce;

// 12.
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const Frankie = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(Frankie);
